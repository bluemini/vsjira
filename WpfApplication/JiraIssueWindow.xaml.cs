﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VSJira.Core;
using VSJira.Core.UI;

namespace WpfApplication
{
    /// <summary>
    /// Interaction logic for JiraIssueWindow.xaml
    /// </summary>
    public partial class JiraIssueWindow : MetroWindow
    {
        public JiraIssueWindow()
        {
            var services = new VSJiraServices()
            {
                JiraFactory = new TestJiraFactory(),
                WindowFactory = new WindowFactory(),
                VisualStudioServices = new MockVisualStudioServices()
            };

            var jira = new TestJiraFactory().Create("http://foo", "admin", "admin");
            var issue = jira.Issues.GetIssuesFromJqlAsync("jql").Result.First();
            var viewModel = new JiraIssueViewModel(issue, services);

            Initialize(viewModel, services);
        }

        public JiraIssueWindow(JiraIssueViewModel viewModel, VSJiraServices services)
        {
            Initialize(viewModel, services);
        }

        private void Initialize(JiraIssueViewModel viewModel, VSJiraServices services)
        {
            InitializeComponent();
            var vm = new JiraIssueUserControlViewModel();
            this.JiraIssueControl.Content = new JiraIssueUserControl(vm);
            vm.Initialize(viewModel, services);

            var theme = services.VisualStudioServices.GetConfigurationOptions().Theme;
            ThemeManager.Apply(this.Resources, theme);
        }
    }
}
