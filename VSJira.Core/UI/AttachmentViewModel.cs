﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class AttachmentViewModel : ViewModelBase
    {
        private readonly VSJiraServices _services;
        private readonly Attachment _attachment;

        public DelegateCommand OpenFileCommand { get; private set; }

        public AttachmentViewModel(Attachment attachment)
        {
            _attachment = attachment;
            this.OpenFileCommand = new DelegateCommand(async () => await this.OpenFileAsync());
        }

        public string FileName
        {
            get
            {
                return this._attachment.FileName;
            }
        }

        public string Author
        {
            get
            {
                return this._attachment.Author;
            }
        }

        public DateTime? CreatedDate
        {
            get
            {
                return this._attachment.CreatedDate;
            }
        }

        private async Task OpenFileAsync()
        {
            var tempPath = Path.GetTempPath();
            var tempFileName = Path.Combine(tempPath, this._attachment.FileName);

            try
            {
                await this._attachment.DownloadAsync(tempFileName);
                System.Diagnostics.Process.Start(tempFileName);
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this._services.WindowFactory.ShowMessageBox("Failed to retrieve attachment.", ex);
            }
        }
    }
}
