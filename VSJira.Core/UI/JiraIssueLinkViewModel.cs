﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraIssueLinkViewModel : JiraIssueViewModel
    {
        public JiraIssueLinkViewModel(string linkDescription, Issue issue, VSJiraServices services)
            : base(issue, services)
        {
            LinkDescription = linkDescription;
        }

        public string LinkDescription { get; set; }
    }
}
