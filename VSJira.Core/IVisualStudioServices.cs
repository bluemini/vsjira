﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core.UI;

namespace VSJira.Core
{
    public class OptionsChangedEventArgs : EventArgs
    {
        public IJiraOptions Options { get; private set; }

        public OptionsChangedEventArgs(IJiraOptions options)
        {
            this.Options = options;
        }
    }

    public interface IVisualStudioServices
    {
        IJiraOptions GetConfigurationOptions();
        void ShowOptionsPage();
        void ShowJiraIssueToolWindow(JiraIssueViewModel viewModel, VSJiraServices services);
        event EventHandler<OptionsChangedEventArgs> OptionsChanged;
        void NotifyOptionsChanged(IJiraOptions options);
    }
}
