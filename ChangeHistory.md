# Change History

** Version 2.6.0 (01/19/2017) **

* Add ability to create work logs on an issue.
* Add ability to list linked issues.
* Drop support for VS2013.

** Version 2.5.0 (01/0/2017) **

* Add ability to list work logs of an issue.
* Add ability to copy error details to clipboard.

** Version 2.4.0 (12/14/2015) **

* Add ability to list and download attachments.

** Version 2.3.0 (12/11/2015) **

* Add ability to create issues from whithin the issues tool window.

** Version 2.2.0 (12/09/2015) **

* Add ability to execute a workflow transition on an issue.

** Version 2.1.0 (12/05/2015) **

* Add ability to view and edit comments on issues.

** Version 2.0.0 (12/02/2015) **

* Add ability to switch between Light and Dark themes. Fixes https://bitbucket.org/farmas/vsjira/issues/6.

** Version 1.3.0 (11/26/2015) **

* Add ability to update JIRA issue fields within the tool window.
- Use current culture to format dates. Fixes https://bitbucket.org/farmas/vsjira/issues/7. 

** Version 1.2.0 (11/25/2015) **

* Add a multi-instance VisualStudio tool window to show read-only information of an issue.